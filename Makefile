.PHONY: build start

build:
	docker build -t thriflora .

start: build
	docker run --rm -it -v ${pwd}:/thriflora -p 4200:4200 thriflora
