FROM node:16-slim
WORKDIR /thriflora
COPY package.json package-lock.json /thriflora/
RUN npm install
COPY . /thriflora
CMD npm run start
