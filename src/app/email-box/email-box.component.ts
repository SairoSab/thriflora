import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-email-box',
  templateUrl: './email-box.component.html',
  styleUrls: ['./email-box.component.scss']
})
export class EmailBoxComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  validateEmail(field: string) {
    var regex=/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i;
    return (regex.test(field)) ? true : false;
  }
}
